<?php
require_once "animal.php";
require_once "ape.php";
require_once "frog.php";

$sheep = new Animal("shaun");

echo "Name: $sheep->name <br>"; // "shaun"
echo "Legs: $sheep->legs <br>"; // 2
echo "Cold Blooded: $sheep->cold_blooded <br>"; // false
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name: $sungokong->name <br>";
echo "Function yell: ";
$sungokong->yell(); // "Auooo"
echo "<br> <br>";

$kodok = new Frog("buduk");
echo "Name: $kodok->name <br>";
echo "Legs: $kodok->legs <br>";
echo "Function jump: ";
$kodok->jump(); // "hop hop"
